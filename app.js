const app = Vue.createApp({
  data() {
    return { 
      goals: [], goalname:'' };
  },

  methods: {
    addgoal(){
      this.goals.push(this.goalname);
      this.goalname='';
    },
    removegoal(indx){
      this.goals.splice(indx,1);
    },
  },
});

app.mount('#user-goals');

